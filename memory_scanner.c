///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 04 - MemoryScanner
///
/// @file memory_scanner.c
/// @version 1.0
///
/// @author chadMorita <chadmmm@hawaii.edu>
/// @brief  Lab 04 - MemoryScanner - EE 491F - Spr 2021
/// @date   10_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include "memory_scanner.h"
#include "reader.h"

int main() {

   FILE * file_pointer; // File pointer for the maps file
   struct memory_segment mem_seg; // Used to temporarily hold info for one line of the maps file
   int ret_val; // Temporarily store the return value of read_line
   int line_num = 0; // Keeps track of the number of lines printed

   // Try to open the maps file for reading
   file_pointer = fopen("/proc/self/maps", "r");

   // Check that the file was able to be opened
   if (file_pointer == NULL) {
      fprintf(stderr, "Error: unable to open file: /proc/self/maps\n");
      return EXIT_FAILURE;
   } 
   

   // Continue reading lines until the end of the file
   while ( (ret_val = read_line(&mem_seg, file_pointer)) != 2) {
      if (ret_val == 0) {
         // Scan the memory and print the info
         scan_memory(&mem_seg, line_num);
         
         // Keep track of the number of printed lines
         line_num++;
      }
   }

   return 0;
}
