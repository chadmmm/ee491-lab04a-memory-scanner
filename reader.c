///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 491 - Software Reverse Engineering
///// Lab 04 - MemoryScanner
/////
///// @file reader.c
///// @version 1.0
/////
///// @author chadMorita <chadmmm@hawaii.edu>
///// @brief  Lab 04 - MemoryScanner - EE 491F - Spr 2021
///// @date   10_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "memory_scanner.h"

const void * MAX_ADDRESS = (void *) 0x00007ff000000000;

/*
 * @brief Reads in one line of the maps file and adds the information into a
 * struct.
 *
 * @param struct memory_segment * mem_seg A struct to hold the information for 
 *    one line of the maps file
 * @param FILE * file_pointer The file pointer to the file. Must be pointing to
 *    the correct place in the file.
 * @return int 0 success, 1 incomplete struct, 2 end of file
 */
int read_line(struct memory_segment * mem_seg, FILE * file_pointer) {

   int c; // Temp variable to hold character from fgetc
   char address[13]; // Temp char arraty to store address being read in
   int counter = 0; // Variable to hole character counter
   bool valid_entry = false; // Kepp track of whether a valid line is read

   // **Read start address**

   // Try to get the start address
   while ((c = getc(file_pointer)) != EOF ) {

      // Exit once a '-' is encountered
      if (c == '-') { break; }
     
      // Build a string with the read in characters
      address[counter] = c;
      
      // Keep track of the number of characters read
      counter++;

      // If the address is longer than 48 bits, return
      if (counter > 12) { return 1; }
   }

   // Only convert the address if the end of file hasn't been reached
   if (c != EOF) {
      // Null terminate to make it a string
      address[12] = '\0';

      // Covvert from a string to an address
      sscanf(address, "%p", &mem_seg->start_address);
   }
  


   // **Read end address**
   
   // Reset counter
   counter = 0;

   // Try to get the start address
   while ((c = getc(file_pointer)) != EOF ) {

      // Exit once a space is encountered
      if (c == ' ') { break; }
     
      // Build a string with the read in characters
      address[counter] = c;
      
      // Keep track of the number of characters read
      counter++;

      // If the address is longer than 96 bits, exit
      if (counter > 12) { return 1; }
   }


   // Only convert the address if the end of file hasn't been reached
   if (c != EOF) {
      // Null terminate to make it a string
      address[12] = '\0';

      // Convert from a string to an address
      sscanf(address, "%p", &mem_seg->end_address);
   }

   // Read one character of permissions
   c = getc(file_pointer);
   
   // Set appropriate read flag
   if (c == 'r') {
      mem_seg->read = true;
   } else {
      mem_seg->read = false;
   }

   // Read one character of permissions
   c = getc(file_pointer);
   
   // Set appropriate write flag
   if (c == 'w') {
      mem_seg->write = true;
   } else {
      mem_seg->write = false;
   }
   
   // Read one character of permissions
   c = getc(file_pointer);
   
   // Set appropriate execute flag
   if (c == 'x') {
      mem_seg->execute = true;
   } else {
      mem_seg->execute = false;
   }
   
   // Read one character of permissions
   c = getc(file_pointer);
   
   // Set appropriate shared flag
   if (c == 's') {
      mem_seg->shared = true;
   } else {
      mem_seg->shared = false;
   }


   // Fast forward to the next line or end of file
   while ((c = getc(file_pointer)) != EOF ) {
      if (c == '\n')  {
         if ( (mem_seg->read == true) && (mem_seg->start_address < MAX_ADDRESS) ) {
            valid_entry = true;
         }

         // Break out of the loop when a newline is encountered
         break;
      }
   }





   // If the end of file is reached, return 2
   if (c == EOF) {
      return 2;
      
   // If a valid line is read, return 0
   } else if (valid_entry == true) {
      return 0;

   // If an invalid line is read, return 1
   } else {
      return 1;
   }
}

/*
 * @brief Scans the regions of memory defined in the struct and print 
 *    information about the file
 * @param struct memory_segment * mem_seg A struct to hold the information for 
 *    one line of the maps file
 * @param int line_num The current line number to be printed
 * @return none
 */
void scan_memory(struct memory_segment * mem_seg, int line_num) {
   int num_bytes; // Store the number of bytes read
   int num_a = 0; // Counter for the number of 'A's

   // Count the number of 'A's
   for (void * i = mem_seg->start_address; i < mem_seg->end_address; i++) {
      if ( *( (unsigned char *) i) == 'A') {
        num_a++; 
      }
      // Keep track of the number of bytes read
      num_bytes++;
   }

   // Print the start and end address
   printf("%d:  %p - %p  ", line_num, mem_seg->start_address, mem_seg->end_address);

   // Print the file permissions
   if (mem_seg->read) {
      printf("r");
   } else {
      printf("-");
   }
   if (mem_seg->write) {
      printf("w");
   } else {
      printf("-");
   }
   if (mem_seg->execute) {
      printf("x");
   } else {
      printf("-");
   }
   if (mem_seg->shared) {
      printf("s");
   } else {
      printf("p");
   }
   printf("  ");

   // Print the number of bytes read and number of 'A's
   printf("Number of bytes read [%d]   Number of 'A' is [%d]\n", num_bytes, num_a);

}


