#ifndef MEMORY_SCANNER_H
#define MEMORY_SCANNER_H

///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 04 - MemoryScanner
///
/// @file memory_scanner.h
/// @version 1.0
///
/// @author chadMorita <chadmmm@hawaii.edu>
/// @brief  Lab 04 - MemoryScanner - EE 491F - Spr 2021
/// @date   10_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdbool.h>

struct memory_segment {
   void * start_address;
   void * end_address;
   bool read;
   bool write;
   bool execute;
   bool shared;
};



#endif
