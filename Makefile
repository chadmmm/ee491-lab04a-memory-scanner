# Build a Memory Scanning program

CC     = gcc
CFLAGS = -c -g -Wall

TARGET = memory_scanner

all: $(TARGET)

$(TARGET): $(TARGET).o reader.o
	$(CC) -o $(TARGET) $(TARGET).o reader.o

$(TARGET).o: $(TARGET).c
	$(CC) $(CFLAGS) $(TARGET).c

reader.o: reader.c
	$(CC) $(CFLAGS) reader.c


test: $(TARGET)
	./$(TARGET)

clean:
	rm $(TARGET) *.o

