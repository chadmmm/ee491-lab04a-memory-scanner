#ifndef READER_H
#define READER_H

///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 04 - MemoryScanner
///
/// @file reader.h
/// @version 1.0
///
/// @author chadMorita <chadmmm@hawaii.edu>
/// @brief  Lab 04 - MemoryScanner - EE 491F - Spr 2021
/// @date   10_02_2021
///////////////////////////////////////////////////////////////////////////////

int read_line(struct memory_segment *, FILE *);
int scan_memory(struct memory_segment *, int);


#endif
